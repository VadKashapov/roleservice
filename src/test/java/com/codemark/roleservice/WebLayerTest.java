package com.codemark.roleservice;

import com.codemark.roleservice.Model.Account;
import com.codemark.roleservice.Model.Role;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class WebLayerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void notFoundTest() throws Exception {
        mvc.perform(get("/accounts/{login}", "ods2131")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void addAccTest() throws Exception {
        Account acc = new Account("223ds", "Passw1", "Kiod");
        String body = objectMapper.writeValueAsString(acc);

        mvc.perform(post("/accounts")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.success").value(true))
                .andDo(MockMvcResultHandlers.print());

        mvc.perform(get("/accounts/{login}",acc.getLogin())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login").value(acc.getLogin()))
                .andExpect(jsonPath("$.password").value(acc.getPassword()))
                .andExpect(jsonPath("$.name").value(acc.getName()))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void addAccPasswordNotValidTest() throws Exception {
        Account acc = new Account("223ds", "passw1", "Kiod");
        String body = objectMapper.writeValueAsString(acc);

        mvc.perform(post("/accounts")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.errors[0].message").value("must have at least 1 uppercase letter and 1 digit"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void passwordRegexTest() throws Exception {
        String pattern = "^.*(?:(\\p{javaUpperCase}.*\\d)|(\\d.*\\p{javaUpperCase})).*$";

        Assert.assertTrue("2oooBo".matches(pattern));
        Assert.assertTrue("ooBoo2".matches(pattern));
        Assert.assertTrue("ooB2oo".matches(pattern));
        Assert.assertFalse("BoooBo".matches(pattern));
        Assert.assertFalse("2oo2oo".matches(pattern));
        Assert.assertFalse("oooooo".matches(pattern));
        Assert.assertFalse("ooBooo".matches(pattern));
        Assert.assertFalse("ooooo2".matches(pattern));
    }

    @Test
    void delAccTest() throws Exception {
        String login = "laos66";

        mvc.perform(delete("/accounts/{login}",login)
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/accounts/{login}",login)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void updateAccTest() throws Exception {
        Account acc = new Account("laos66", "Passw1New", "username_4");
        Set<Role> roles = acc.getRoles();
        roles.add(new Role(145,"Связист"));
        roles.add(new Role(146,"Механик"));
        String body = objectMapper.writeValueAsString(acc);

        mvc.perform(put("/accounts")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success").value(true))
                .andDo(MockMvcResultHandlers.print());

        mvc.perform(get("/accounts/{login}",acc.getLogin())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login").value(acc.getLogin()))
                .andExpect(jsonPath("$.password").value(acc.getPassword()))
                .andExpect(jsonPath("$.name").value(acc.getName()))
                .andExpect(jsonPath("$.roles[0].id").value(145))
                .andExpect(jsonPath("$.roles[0].name").value("Связист"))
                .andExpect(jsonPath("$.roles[1].id").value(146))
                .andExpect(jsonPath("$.roles[1].name").value("Механик"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void updateAccNotFoundTest() throws Exception {
        Account acc = new Account("32", "Passw1New", "username_2");
        Set<Role> roles = acc.getRoles();
        roles.add(new Role(145,"Связист"));
        roles.add(new Role(146,"Механик"));
        String body = objectMapper.writeValueAsString(acc);

        mvc.perform(put("/accounts")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.errors[0].message").value("account to update not found"))
                .andDo(MockMvcResultHandlers.print());

        mvc.perform(get("/accounts/{login}",acc.getLogin())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""))
                .andDo(MockMvcResultHandlers.print());
    }
}