package com.codemark.roleservice.Model;


import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Validated
public class Role implements Serializable {
    @Id
    long id;
    @NotBlank
    String name;
}
