package com.codemark.roleservice.Model;

public interface AccountWithoutRoles {
    String getName();
    String getLogin();
    String getPassword();
}
