package com.codemark.roleservice.Model;

import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@ToString
@Validated
public class Account implements Serializable {
    @Id
    String login;
    @NotBlank
    @Pattern(regexp = "^.*(?:(\\p{javaUpperCase}.*\\d)|(\\d.*\\p{javaUpperCase})).*$",message = "must have at least 1 uppercase letter and 1 digit")
    String password;
    @NotBlank
    String name;
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "account_role",
            joinColumns = { @JoinColumn(name = "acc_id", referencedColumnName = "login") },
            inverseJoinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") }
    )
    Set<Role> roles = new HashSet<>();

    public Account(String login, String password, String name) {
        this.login = login;
        this.password = password;
        this.name = name;
    }
}
