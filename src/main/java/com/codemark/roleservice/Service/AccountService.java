package com.codemark.roleservice.Service;

import com.codemark.roleservice.Model.Account;
import com.codemark.roleservice.Model.AccountWithoutRoles;
import com.codemark.roleservice.Repository.AccountRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class AccountService {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @NonNull
    public List<AccountWithoutRoles> getAccounts() {
        return accountRepository.findAllProjectedBy();
    }

    @NonNull
    public Account getAccountWithRoles(String login) {
        return accountRepository.getAccountWithRolesByLogin(login);
    }

    public int deleteAccountByLogin(String login) {
        return accountRepository.deleteAccountByLogin(login);
    }

    public Account addAccount(Account acc) {
         return accountRepository.save(acc);
    }

    public Account update(Account acc) throws NullPointerException {
        Account accToUpdate = getAccountWithRoles(acc.getLogin());
        accToUpdate.setName(acc.getName());
        accToUpdate.setPassword(acc.getPassword());
        if (!acc.getRoles().isEmpty()) {
            accToUpdate.setRoles(acc.getRoles());
        }
        return accountRepository.save(accToUpdate);
    }
}
