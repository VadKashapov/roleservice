package com.codemark.roleservice.Controller;

import com.codemark.roleservice.Controller.AccountController.ValidationErrorResponse.Violation;
import com.codemark.roleservice.Model.Account;
import com.codemark.roleservice.Model.AccountWithoutRoles;
import com.codemark.roleservice.Service.AccountService;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@Validated
public class AccountController {
    private AccountService service;

    @Autowired
    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping(value = "/accounts")
    public List<AccountWithoutRoles> getAccounts() {
        return checkNull(service.getAccounts());
    }

    @GetMapping(value = "/accounts/{login}")
    public Account getAccountWithRole(@PathVariable @NotBlank String login) {
        return checkNull(service.getAccountWithRoles(login));
    }

    @DeleteMapping(value = "/accounts/{login}")
    public int deleteAccount(@PathVariable @NotBlank String login) {
        return service.deleteAccountByLogin(login);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/accounts")
    public ValidationErrorResponse addAccount(@Valid @RequestBody Account acc) {
        service.addAccount(acc);
        return new ValidationErrorResponse(true);
    }

    @PutMapping(value = "/accounts")
    public ValidationErrorResponse updateAccount(@Valid @RequestBody Account acc) {
        try {
                service.update(acc);
        } catch (NullPointerException e) {
            return new ValidationErrorResponse(false
                    , Arrays.asList(new Violation("-","account to update not found")));
        }
         return new ValidationErrorResponse(true);
    }

    private <T> T checkNull(T o) {
        if (o == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
        }
        return o;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ValidationErrorResponse handleConstraintViolationException(ConstraintViolationException e) {
        List<Violation> violations = new ArrayList<>();
        e.getConstraintViolations().forEach(v -> violations.add(new Violation(v.getPropertyPath().toString(),v.getMessage())));
        return new ValidationErrorResponse(false,violations);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ValidationErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<Violation> violations = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(v -> violations.add(new Violation(v.getObjectName(),v.getDefaultMessage())));
        return new ValidationErrorResponse(false,violations);
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ValidationErrorResponse {

        private Boolean success;
        private String message;
        private List<Violation> errors;

        public ValidationErrorResponse(Boolean success) {
            this.success = success;
        }

        public ValidationErrorResponse(Boolean success, List<Violation> errors) {
            this.success = success;
            this.errors = errors;
        }

        @Getter
        @Setter
        @AllArgsConstructor
        public static class Violation {
            private final String fieldName;
            private final String message;
        }
    }
}
