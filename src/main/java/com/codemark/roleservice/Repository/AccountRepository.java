package com.codemark.roleservice.Repository;

import com.codemark.roleservice.Model.Account;
import com.codemark.roleservice.Model.AccountWithoutRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {
    public List<AccountWithoutRoles> findAllProjectedBy();
    public Account getAccountWithRolesByLogin(String login);
    public int deleteAccountByLogin(String login);
    public Account save(Account account);
    public Account getOne(String login);
}
